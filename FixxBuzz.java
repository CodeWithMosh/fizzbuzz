package CodeWithMosh.FizzBuzz;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Scanner;

class FizzBuzz {
    public static void main(String[] arguments) {
        Scanner scanner = new Scanner(System.in);
        // Enter number
        // If its divisable with 5 = FIZZ
        // If its divisable with 3 = BUZZ
        // If its divisable with both 5 AND 3 == FIZZBUZZ
        // If its not divisable with 5 and 3 == SAME NUMBER

        boolean continuePlaying = true;

        while (continuePlaying) {
            
            System.out.print("Enter number: ");
            int number = scanner.nextInt();

            if (number % 5 == 0) {
                System.out.print("FIZZ");
            }
            if (number % 3 == 0) {
                System.out.print("BUZZ");
            }
            if (number % 5 != 0 && number % 3 != 0) {
                System.out.println(number);
            }
            System.out.println("");
            System.out.println("Play again?");
            String answer = scanner.next();
            
            if (!answer.equalsIgnoreCase("Y") ) {
                continuePlaying = false;
            }
            
        }

    }
}